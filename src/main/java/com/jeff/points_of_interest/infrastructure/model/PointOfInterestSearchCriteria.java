package com.jeff.points_of_interest.infrastructure.model;

public class PointOfInterestSearchCriteria {

  private Double minLat;
  private Double minLon;

  public PointOfInterestSearchCriteria(final Double minLat, final Double minLon) {
    this.minLat = minLat;
    this.minLon = minLon;
  }

  public Double getMinLat() {
    return minLat;
  }

  public Double getMinLon() {
    return minLon;
  }
}
