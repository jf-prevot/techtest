package com.jeff.points_of_interest.infrastructure.service;

public class RoundHalfDownService {

  public Double execute(Double input) {
    final int integerValue = input.intValue();
    final boolean isDecimalPartIsLow = Math.abs(input - input.intValue()) < 0.5;
    if (input > 0) {
      return integerValue + (isDecimalPartIsLow ? 0 : 0.5);
    } else {
      return integerValue - (isDecimalPartIsLow ? 0.5 : 1);
    }
  }
}
