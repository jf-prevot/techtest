package com.jeff.points_of_interest;

import com.google.gson.Gson;
import com.jeff.points_of_interest.controller.PointOfInterestController;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import com.jeff.points_of_interest.infrastructure.service.CommandLineParser;
import com.jeff.points_of_interest.infrastructure.service.MapPoiInZone;
import com.jeff.points_of_interest.infrastructure.service.RoundHalfDownService;
import com.jeff.points_of_interest.service.DensestZoneService;
import com.jeff.points_of_interest.service.NbPoiService;

import java.io.IOException;

public class Main {

  public static void main(String[] args) throws IOException {
    final Gson gson = new Gson();
    final CommandLineParser commandLineParser = new CommandLineParser(gson);
    final PoiDAO poiDAO = new PoiDAO();
    final NbPoiService nbPoiService = new NbPoiService(poiDAO);
    final RoundHalfDownService roundHalfDownService = new RoundHalfDownService();
    final MapPoiInZone mapPoiInZone = new MapPoiInZone(roundHalfDownService);
    final DensestZoneService densestZoneService = new DensestZoneService(poiDAO, mapPoiInZone);
    final PointOfInterestController pointOfInterestController = new PointOfInterestController(commandLineParser, nbPoiService, densestZoneService);

    System.out.println(gson.toJson(pointOfInterestController.executeCommandLine(args)));
  }
}
