package com.jeff.points_of_interest.service;

import com.jeff.points_of_interest.dto.response.NbPoiDTO;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;

import java.io.IOException;

public class NbPoiService {

  private PoiDAO poiDAO;

  public NbPoiService(final PoiDAO poiDAO) {
    this.poiDAO = poiDAO;
  }

  public NbPoiDTO execute(final PointOfInterestSearchCriteria searchCriteria) throws IOException {
    return new NbPoiDTO(poiDAO.findBySearchCriteria(searchCriteria).size());
  }
}
