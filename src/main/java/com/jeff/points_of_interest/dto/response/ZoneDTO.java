package com.jeff.points_of_interest.dto.response;

public class ZoneDTO {

  private Double min_lat;
  private Double max_lat;
  private Double min_lon;
  private Double max_lon;

  public ZoneDTO(final Double min_lat, final Double max_lat, final Double min_lon, final Double max_lon) {
    this.min_lat = min_lat;
    this.max_lat = max_lat;
    this.min_lon = min_lon;
    this.max_lon = max_lon;
  }
}
