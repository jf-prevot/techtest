package com.jeff.points_of_interest.domain.model;

import java.util.Objects;

public class PointOfInterest {

  private String id;
  private Double lat;
  private Double lon;

  public PointOfInterest(final String id, final Double lat, final Double lon) {
    this.id = id;
    this.lat = lat;
    this.lon = lon;
  }

  public Double getLat() {
    return lat;
  }

  public Double getLon() {
    return lon;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof PointOfInterest)) {
      return false;
    }
    final PointOfInterest that = (PointOfInterest) o;
    return id.equals(that.id) &&
           lat.equals(that.lat) &&
           lon.equals(that.lon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, lat, lon);
  }

  @Override
  public String toString() {
    return "PointOfInterest{" +
           "id='" + id + '\'' +
           ", lat=" + lat +
           ", lon=" + lon +
           '}';
  }
}
