package com.jeff.points_of_interest.reading_case;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.dto.response.NbPoiDTO;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import com.jeff.points_of_interest.service.NbPoiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NbPoiServiceTest {

  @Mock
  private PointOfInterest POI_1;
  @Mock
  private PointOfInterest POI_2;
  @Mock
  private PointOfInterestSearchCriteria searchCriteria;
  @Mock
  private PoiDAO poiDAO;
  @InjectMocks
  private NbPoiService nbPoiService;

  @Test
  void execute() throws IOException {
    List<PointOfInterest> expectedPointOfInterests = Arrays.asList(POI_1, POI_2);
    when(poiDAO.findBySearchCriteria(searchCriteria)).thenReturn(expectedPointOfInterests);

    NbPoiDTO nbPoiDTO = nbPoiService.execute(searchCriteria);

    verify(poiDAO).findBySearchCriteria(searchCriteria);
    assertThat(nbPoiDTO).isEqualToComparingFieldByField(new NbPoiDTO(expectedPointOfInterests.size()));
  }
}