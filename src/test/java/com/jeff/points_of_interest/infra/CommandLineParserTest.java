package com.jeff.points_of_interest.infra;

import com.google.gson.Gson;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;
import com.jeff.points_of_interest.infrastructure.service.CommandLineParser;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CommandLineParserTest {

  private CommandLineParser commandLineParser = new CommandLineParser(new Gson());

  @Test
  void parseMaxResult() {
    final Integer maxResult = commandLineParser.parseMaxResult(new String[]{"--densest", "'{\"n\": 3}'"});

    assertThat(maxResult).isEqualTo(3);
  }

  @Test
  void parseZoneParameter() {
    final PointOfInterestSearchCriteria searchCriteria = commandLineParser.parseZoneParameter(new String[]{"--nbpoi", "'{\"min_lat\": -8.6,\"min_lon\": 42}'"});

    assertThat(searchCriteria).isEqualToComparingFieldByField(new PointOfInterestSearchCriteria(-8.6, 42.0));
  }
}