package com.jeff.points_of_interest.infrastructure.service;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.domain.model.Zone;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MapPoiInZoneTest {

  @Mock
  private RoundHalfDownService roundHalfDownService;
  @InjectMocks
  private MapPoiInZone mapPoiInZone;
  @Captor
  private ArgumentCaptor<Double> coordinateCaptor;

  @Test
  void retrieve_the_zone_including_a_point_of_interest() {
    final Zone expectedZone = new Zone(-42.0, -41.5, 66.5, 67.0);
    final Double poiLat = -42.0;
    final Double poiLon = 66.6;
    when(roundHalfDownService.execute(poiLat)).thenReturn(-42.0);
    when(roundHalfDownService.execute(poiLon)).thenReturn(66.5);

    final Zone zone = mapPoiInZone.execute(new PointOfInterest("any id", poiLat, poiLon));

    verify(roundHalfDownService, times(2)).execute(coordinateCaptor.capture());
    assertThat(coordinateCaptor.getAllValues()).containsExactly(poiLat, poiLon);
    assertThat(zone).isEqualToComparingFieldByField(expectedZone);
  }
}