package com.jeff.points_of_interest.infrastructure;

import com.jeff.points_of_interest.infrastructure.service.RoundHalfDownService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RoundHalfDownServiceTest {

  private RoundHalfDownService roundHalfDownService = new RoundHalfDownService();

  @Test
  void lowerPositive() {
    roundHalfDown(48.2, 48.0);
  }

  @Test
  void greaterPositive() {
    roundHalfDown(48.6, 48.5);
  }

  @Test
  void lowerNegative() {
    roundHalfDown(-1.3, -1.5);
  }

  @Test
  void greaterNegative() {
    roundHalfDown(-48.6, -49.0);
  }

  private void roundHalfDown(final Double input, final Double expected) {
    assertThat(roundHalfDownService.execute(input)).isEqualTo(expected);
  }
}