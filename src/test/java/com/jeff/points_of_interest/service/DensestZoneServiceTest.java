package com.jeff.points_of_interest.service;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.domain.model.Zone;
import com.jeff.points_of_interest.dto.response.ZoneDTO;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import com.jeff.points_of_interest.infrastructure.service.MapPoiInZone;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DensestZoneServiceTest {

  private final static PointOfInterest POI1 = new PointOfInterest("id1", -48.6, -37.7);
  private final static PointOfInterest POI2 = new PointOfInterest("id2", -27.1, 8.4);
  private final static PointOfInterest POI3 = new PointOfInterest("id3", 6.6, -6.9);
  private final static PointOfInterest POI4 = new PointOfInterest("id4", -2.3, 38.3);
  private final static PointOfInterest POI5 = new PointOfInterest("id5", 6.8, -6.9);
  private final static PointOfInterest POI6 = new PointOfInterest("id6", -2.5, 38.3);
  private final static PointOfInterest POI7 = new PointOfInterest("id7", 0.1, -0.1);
  private final static PointOfInterest POI8 = new PointOfInterest("id8", -2.1, 38.1);
  private final static List<PointOfInterest> ALL_POIS = Arrays.asList(POI1, POI2, POI3, POI4, POI5, POI6, POI7, POI8);
  private final Zone ZONE1 = new Zone(-49.0, -48.5, -38.0, -37.5);//-48.6, -37.7);
  private final Zone ZONE2 = new Zone(-27.5, -27.0, 8.0, 8.0);//-27.1, 8.4);
  private final Zone ZONE3 = new Zone(6.5, 7.0, -7.0, -6.5);//6.6, -6.9);
  private final Zone ZONE4 = new Zone(-2.5, -2.0, 38.0, 38.5);//-2.3, 38.3);
  private final Zone ZONE5 = new Zone(6.5, 7.0, -7.0, -6.5);//6.8, -6.9);
  private final Zone ZONE6 = new Zone(-2.5, -2.0, 38.0, 38.5);//-2.5, 38.3);
  private final Zone ZONE7 = new Zone(0.0, 0.5, -0.5, -0.0);//0.1, -0.1);
  private final Zone ZONE8 = new Zone(-2.5, -2.0, 38.0, 38.5);//-2.1, 38.1);
  @Mock
  private PoiDAO poiDAO;
  @Mock
  private MapPoiInZone mapPoiInZone;
  @InjectMocks
  private DensestZoneService densestZoneService;

  @Test
  void retrieve_the_x_densest_zones() throws IOException {
    when(poiDAO.getAll()).thenReturn(ALL_POIS);
    when(mapPoiInZone.execute(POI1)).thenReturn(ZONE1);
    when(mapPoiInZone.execute(POI2)).thenReturn(ZONE2);
    when(mapPoiInZone.execute(POI3)).thenReturn(ZONE3);
    when(mapPoiInZone.execute(POI4)).thenReturn(ZONE4);
    when(mapPoiInZone.execute(POI5)).thenReturn(ZONE5);
    when(mapPoiInZone.execute(POI6)).thenReturn(ZONE6);
    when(mapPoiInZone.execute(POI7)).thenReturn(ZONE7);
    when(mapPoiInZone.execute(POI8)).thenReturn(ZONE8);

    List<ZoneDTO> zoneDTOS = densestZoneService.execute(2);
  }
}